
public class Ques_1 {
	
   public static void main(String[] args) {
	   	int max=0;
	   	int num;
	   	for (String arg:args) {
	   		num = Integer.parseInt(arg);
	   		if (num>max) {
	   			max=num;
	   		}
	   	}
	   	System.out.println(max);
}
}
