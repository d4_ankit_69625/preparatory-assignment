//Write a program to input n numbers on command line argument and calculate maximum of them
#include<stdio.h>

int main(int argc, char * argv[]){
    int i, n, large;
    n = argc;
    if(argc == 1)
        exit(0);
    large = atoi(argv[1]);
    // Loop to store largest number to large
    for(i = 2; i < n; i++)
    {
       if(large < atoi(argv[i]))
           large = atoi(argv[i]);
    }
    //print statement should be according to Question
    printf("Largest element = %d", large);

    return 0;
}