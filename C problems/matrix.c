#include<stdio.h>
int main()
{
    int a[2][2], b[2][2], c[2][2], n, i, j, k;
    printf("Enter the value of N (N<=2) :");
    scanf("%d",&n);

    printf("Enter the elements of matrix A :");
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            scanf("%d",&a[i][j]);
        }
    }
    printf("Enter the elements of matrix B :");
    for(i=0; i<=n; i++)
    {
        for(j=0; j<n; j++)
        {
            scanf("%d",&b[i][j]);
        }
    }
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            c[i][j]=0;
            for(k=0; k<n; k++)
            {
                c[i][j]+= a[i][k]*b[k][j];
            }
        }
    }
    printf("The product of two matrices is :");
    for(i=0; i<n; i++)
    {
        for(j=0; j<n; j++)
        {
            printf("%d\t",c[i][j]);
        }
        printf("\n");
    }
    return 0;
}