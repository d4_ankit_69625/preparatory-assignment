import java.util.Scanner;
public class Ques_3 {

	public static void main(String[] args) {
		// Fibonacci number
		Scanner sc= new Scanner(System.in);
		int n = sc.nextInt(); 
		int a = 0, b = 1, sum=0;
	    System.out.println("Fibonacci till " + n + " number");

	    while(sum<n) {
			   System.out.print(sum +", ");
			   a=b;
			   b=sum;
			   sum = a+b;
			   		
		   }
	}
	    
	}

