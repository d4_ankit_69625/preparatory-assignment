
public class Ques_11 {
	    private String firstName;
	    private String lastName;
	    private double monthlySalary;

	    public Ques_11(String firstName, String lastName, double monthlySalary) {
	        this.firstName = firstName;
	        this.lastName = lastName;
	        if (monthlySalary >= 0) {
	            this.monthlySalary = monthlySalary;
	        }
	    }

	    public void setFirstName(String firstName) {
	        this.firstName = firstName;
	    }

	    public String getFirstName() {
	        return firstName;
	    }

	    public void setLastName(String lastName) {
	        this.lastName = lastName;
	    }

	    public String getLastName() {
	        return lastName;
	    }

	    public void setMonthlySalary(double monthlySalary) {
	        if (monthlySalary >= 0) {
	            this.monthlySalary = monthlySalary;
	        }
	    }

	    public double getMonthlySalary() {
	        return monthlySalary;
	    }
		
		 public static void main(String[] args) {
			 
		        Ques_11 Ankit = new Ques_11("Ankit", "Singh", 9696);
		        Ques_11 Pratik = new Ques_11("Pratik", "Banarse", 8595);
		        System.out.println(Ankit.getFirstName() + "'s monthly salary is " + Ankit.getMonthlySalary());
		        System.out.println(Pratik.getFirstName() + "'s monthly salary is " + Pratik.getMonthlySalary());
		        Ankit.setMonthlySalary(Ankit.getMonthlySalary() * 1.1);
		        Pratik.setMonthlySalary(Pratik.getMonthlySalary() * 1.1);
		        System.out.println("Salary after 10% boost");
		        System.out.println(Ankit.getFirstName() + "'s monthly salary is " + Ankit.getMonthlySalary());
		        System.out.println(Pratik.getFirstName() + "'s monthly salary is " + Pratik.getMonthlySalary());
		    }
		
	}

